#include <stdio.h>
#include "bw_trig.h"

//void trig(char * data, int nch, int ndp, int * adc, unsigned long * time) 
double bw_trig(double *array1, int ndp)//, int trigON, int Where) 
{
  int i;
  int THR = 50;
  //int THR = 30;
  double Where;
  for(i = 0; i< ndp; i++){
    
      if(i > 1){

        if((array1[i-1] - THR)*(array1[i-2] - THR) <=0){
        
            if(array1[i] > array1[i-2] + 0.01*THR){
                Where = i;
                //cout << Where << endl;
                break;
            }
            
            else{
                Where = 0;
            }
        }
    }
  }
  return Where;
}
