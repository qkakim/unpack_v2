from distutils.core import setup, Extension
import numpy.distutils.misc_util

setup(
    ext_modules=[Extension("bw_Trig", ["bw_Trig.c", "bw_trig.c"])],
    include_dirs=numpy.distutils.misc_util.get_numpy_include_dirs(),
)
