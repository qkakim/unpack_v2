#include <stdio.h>
#include "trig.h"

//void trig(char * data, int nch, int ndp, int * adc, unsigned long * time) 
int trig(int *array1, int ndp)//, int trigON, int Where) 
{
  int i;
  int SigS = 0;
  int PedS = 0;
  int Where;
  for(i = 0; i< ndp; i++){
    SigS = SigS + array1[i];
    if(i >= 10){
      SigS = SigS - array1[i-10];
    }
    if(i >= 110){
      PedS = PedS + array1[i-110];
    }
    if(i >= 210){
      PedS = PedS -array1[i-210];
      /* printf("PedS\n"); */
      /* printf("%d\n", PedS/100); */
      /* printf("SigS\n"); */
      /* printf("%d\n", SigS/10); */
      if((SigS/10 - PedS/100) > 1000){
	    Where = i;
        break;
      }
      else{
	    Where = 0;
      }
    }
  }
  return Where;
}
