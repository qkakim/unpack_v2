
# File information

This codes is unpacking GZIP in python using the 'C++ wrapping function'

And making PYROOT File (.root format)

Need a python3 and ROOT

-----------------------------------------------------------------------
# How to using this file

python wrapping method is 

<u>$python ${setup.py} build_ext --inplace</u>
-----------------------------------------------------------------------
# Update information
1.20190806
 + modified butterworth bandpass filter trigger
 - the formal trigger logic trigged pedestal events. because when use butterworth bandpass filter trigger, offset always set zeros.
 - If baselineRMS was large than trigger thresold, beforce trigger event get pedestal events.
 - **This updated trigger Logic** has **movingaverage** logic
 

2.20200105 -- __version undate -> v2.0.0__
 + modify 
 - 1. remove long deadtime (0.17s -> 0.01s) while unpacking and trigger
 - 2. multichannel trigger
 - 3. simplify code for unpack&trigger (making module)
 --composed to 'function code', 'unpack and trigger code for each set', 'shell script for QJOB', 'control code'

 3.20200107 --debugging-
 + modify
 - 1. modified part of QJOB 
 - 2. qsub -q time shellscript but cannot add more argv option after shell script
 - 3. So, I added some function of making 'for loop' of each shell script**main control.py**

 4.20200304 --debugging-
 + modify
 - 1. modified multichannel trigger
 + added
 - 1. imfomation of which channel trigger
 - 2. photon channel trigger logic, UnpackFunc-v2-photon.py 
