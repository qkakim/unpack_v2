#include <stdio.h>
#include <Python.h>
#include <numpy/arrayobject.h>
#include "unpack.h"

static char module_docstring[] =
  "This module provides an interface for calculating chi-squared using C.";
static char chi2_docstring[] =
  "Calculate the chi-squared of some data given a model.";

static PyObject *Unpack(PyObject *self, PyObject *args);

static PyMethodDef module_methods[] = {
  {"Unpack", Unpack, METH_VARARGS, chi2_docstring},
  {NULL, NULL, 0, NULL}
};

static struct PyModuleDef chi2module = {
  PyModuleDef_HEAD_INIT,
  "Unpack",   /* name of module */
  chi2_docstring, /* module documentation, may be NULL */
  -1,       /* size of per-interpreter state of the module,
	       or -1 if the module keeps state in global variables. */
  module_methods
};

PyMODINIT_FUNC PyInit_Unpack(void)
{
  PyObject *m = PyModule_Create(&chi2module);
  if (m == NULL)
    return;

  /* Load `numpy` functionality. */
  import_array();
  return m;
}

static PyObject *Unpack(PyObject *self, PyObject *args)
{
  PyByteArrayObject * data = NULL;
  int size;

  PyObject * adc;
  PyObject * time;

  if (!PyArg_ParseTuple(args, "YiOO", &data, &size, &adc, &time))
    return NULL;

  int nch = 16;
  int ndp = (int)(size/64);

  char * raw = PyByteArray_AsString((PyObject*) data);

  PyObject *adc_array = PyArray_FROM_OTF(adc, NPY_INT, NPY_IN_ARRAY);
  PyObject *time_array = PyArray_FROM_OTF(time, NPY_ULONG, NPY_IN_ARRAY);

  int * c_adc = (int*)PyArray_DATA(adc_array);
  unsigned long * c_time = (unsigned long *)PyArray_DATA(time_array);

  unpack(raw, nch, ndp, c_adc, c_time);

  Py_DECREF(adc_array);
  Py_DECREF(time_array);

  PyObject *ret = Py_BuildValue("OO", adc, time);

  return ret;
}

