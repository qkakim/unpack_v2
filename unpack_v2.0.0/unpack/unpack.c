#include <stdio.h>
#include "unpack.h"

void unpack(char * data, int nch, int ndp, int * adc, unsigned long * time) 
{
   unsigned long ltmp, coarsetime; 
   unsigned int itmp; 

   int i, j, k;

   for (j = 0; j < ndp; j++) { 
     k = j*64; 
     for (i = 0; i < nch; i++) { 
       adc[i*ndp +j] = data[k + i*3] & 0xFF; 
       itmp = data[k + i*3 + 1] & 0xFF; 
       adc[i*ndp +j] = adc[i*ndp +j] + (unsigned int)(itmp << 8); 
       itmp = data[k + i*3 + 2] & 0xFF; 
       adc[i*ndp +j] = adc[i*ndp +j] + (unsigned int)(itmp << 16); 
     } 

     // get local starting coarse time 
     ltmp = data[k + 48] & 0xFF; 
     coarsetime = ltmp*1000; 
     ltmp = data[k + 49] & 0xFF; 
     ltmp = ltmp << 8; 
     coarsetime = coarsetime + ltmp*1000; 
     ltmp = data[k + 50] & 0xFF; 
     ltmp = ltmp << 16; 
     coarsetime = coarsetime + ltmp*1000; 
     ltmp = data[k + 51] & 0xFF; 
     ltmp = ltmp << 24; 
     coarsetime = coarsetime + ltmp*1000; 
     ltmp = data[k + 52] & 0xFF; 
     ltmp = ltmp << 32; 
     coarsetime = coarsetime + ltmp*1000; 
     ltmp = data[k + 53] & 0xFF; 
     ltmp = ltmp << 40; 
     coarsetime = coarsetime + ltmp*1000; 

     time[j] = coarsetime; 
   } 
}
