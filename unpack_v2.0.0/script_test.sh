##!/bin/bash

source /share/amore/anaconda3/etc/profile.d/conda.sh
source /share/amore/ROOT/6.14.04-v01.01/bin/thisroot.sh

conda activate v01

work_dir=/data/AMoRE/users/kimwootae/work/version_test/unpack_v2/unpack_v2.0.0
executable_name="unpack_v2.py"
log_fils=test.log
path=/data/AMoRE/user/sckim/RODY/AMoRE_ADC/20190305/
subrunNum=1
exe="python ${work_dir}/${executable_name} ${path} ${subrunNum}"

echo ${exe}

${exe}

#exit
