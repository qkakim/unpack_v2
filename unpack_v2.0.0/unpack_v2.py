import os
import sys
import glob
import numpy as np
import threading
import time

from array import array

import gzip
#import Unpack
#import bw_Trig
#import UnpackFunc_v2_photon as upf
import UnpackFunc_v2 as upf

var1 = sys.argv[1]
var2 = sys.argv[2]
path = var1
subrunNum = var2
gzfiles = path + "*gz*"
files = sorted(glob.glob(gzfiles), key=os.path.getmtime)
n = len(files)
i = 0
infiles = []
tfileGrup = []

for filenames in files :
    infiles.append(filenames)
    if(len(infiles) == 50) : 
        i += 1
        tfileGrup.append(infiles)
        infiles = []
    elif((len(files)//50) == i and (len(files)%50 == len(infiles))) :
        tfileGrup.append(infiles)

if __name__ == '__main__' :
    upf.UnpackROOT(tfileGrup[int(subrunNum)], subrunNum)


