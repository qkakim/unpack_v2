##!/bin/bash

args=("$@")
path=${args[0]}
subrunNum=${args[1]}

source /share/amore/anaconda3/etc/profile.d/conda.sh
source /share/amore/ROOT/6.14.04-v01.01/bin/thisroot.sh

conda activate v01

work_dir=/data/AMoRE/users/kimwootae/work/version_test/unpack_v2/unpack_v2.0.0
executable_name="unpack_v2.py"
log_fils=test.log
exe="python ${work_dir}/${executable_name} ${path} ${subrunNum}"

echo ${exe}

${exe}

#exit
