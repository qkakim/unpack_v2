import numpy as np
import os
import sys
import glob
import datetime
import time

from scipy.signal import butter, lfilter
from array import array

from ROOT import TFile, TChain, TDatime, TTree

import gzip
from unpack import Unpack
from Trigg import bw_Trig


'''butterworth'''
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5*fs
    low = lowcut/nyq
    high = highcut/nyq
    b,a = butter(order, [low, high], btype='band')
    return b,a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5) :
    b,a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b,a,data)
    return y

'''UNPAKCING GZIP'''
def buf_cal(input_file) :
    #print (input_file)
    f = gzip.GzipFile(input_file)

    buf = f.read(4)
    nadc = buf[0] & 0xFF
    nadc += (buf[1] & 0xFF) << 8 
    nadc += (buf[2] & 0xFF) << 16
    nadc += (buf[3] & 0xFF) << 24

    buf = f.read(4)
    chunksize = buf[0] & 0xFF
    chunksize += (buf[1] & 0xFF) << 8
    chunksize += (buf[2] & 0xFF) << 16
    chunksize += (buf[3] & 0xFF) << 24

    ntp = int(chunksize/64)

    return f, nadc, chunksize, ntp

'''TRIGGER AND WRITE ROOT'''
def UnpackROOT(filenames, subrunNum) :
    maxn = 25000
    nch = 16
    lowcut = 30
    highcut = 220
    sampling_rate = 100e+3
    nsamp = 0

    t = [[] for _ in range(nch)]
    ch = [[] for _ in range(nch)]
    trgon = [[] for _ in range(nch)]

    aadc = array('i', nch*[0])
    evt_a = array('i', nch*[0])
    n_a = array('i', nch*[0])

    for i in range(nch) :
        t[i] = array('f', maxn*[0])
        ch[i] = array('i', maxn*[0])
        trgon[i] = array('i', [0])

    #fout = TFile("test.root", "recreate")
    fout = TFile("/data/AMoRE/users/kimwootae/work/version_test/test_data/heat_315_{0:05d}.root".format(int(subrunNum)), "recreate")
    tr = TTree("event", "1st Production event")

    for ii in range(nch) :
        tr.Branch("ch_{0}".format(ii), ch[ii], "ch_{0}[25000]/I".format(ii))
        tr.Branch("t_{0}".format(ii), t[ii], "t_{0}[25000]/F".format(ii))
        tr.Branch("trgon_{0}".format(ii), trgon[ii], "trgon_{0}[1]/I".format(ii))
    
    trg = [False for _ in range(nch)] # whether trg or not
    t_adc = [[] for _ in range(int(nch/2))]  # ADC total array
    rtime = [[] for _ in range(int(nch/2))]  # time total array
    n_adc = [[] for _ in range(int(nch/2))]  # ADC total array
    bw = [[] for _ in range(nch)]     # butterworth filter array
    bw_x = [[] for _ in range(nch)]   # butterworth filter trigger x
    x = [[] for _ in range(nch)]      # phonon channel array
    y = [[] for _ in range(nch)]      # photon channel array
    tt = [[] for _ in range(nch)]     # time array
    
    iLoop = 0
    start_execute = time.time()
    filenames = np.array(filenames)
    #for i in range(len(aaa)) : 
        #print (aaa[i])
    for inputfile in filenames :
        #print (inputfile)
        '''Get buffer and chunksize'''
        f, buf, chunksize, ntp = buf_cal(inputfile)
        '''Unpacking and Triggering'''
        while True :
            iLoop += 1
            if(iLoop%10000 == 0) :
                inter_execute = time.time()
                inter_run_time = inter_execute - start_execute
                print ("{0}, run time : {1}".format(iLoop, inter_run_time))
        #for i in range(200) :
            buf = f.read(chunksize)
        
            if len(buf) == 0 :
                break

            adc = np.zeros(shape=(nch, ntp), dtype='i')
            unpack_time = np.zeros(ntp, dtype='uint64')
            
            '''UNPACKING'''
            Unpack.Unpack(bytearray(buf), chunksize, adc, unpack_time)
            #print (chunksize)
            
            for j in range(1,nch,2) : 
                jj = int(j/2)
                t_adc[jj].append(adc[j-1])
                n_adc[jj].append(adc[j])
                rtime[jj].append(unpack_time)

                if(len(n_adc[jj]) <= 14) : # pass len(ADC_array) below 11
                    continue
            
                if(len(n_adc[jj]) > 14) :  # keeping the waveform Recording Length (~2048*11 us)
                    del n_adc[jj][0]
                    del t_adc[jj][0]
                    del rtime[jj][0]

                '''butterworth bandpass filter Trigger'''
                bw[j] = butter_bandpass_filter(n_adc[jj][1]-n_adc[jj][1][0], lowcut, highcut, sampling_rate, order=1)
                bw_x[j] = bw_Trig.bw_Trig(bw[j])

                '''Write triggered data to ROOT file'''
                if ((bw_x[j] > 0) and (trg[j]==False)) :
                    trg[j] = True
                    trgon[j][0] = 1
                    trgon[j-1][0] = 1
                    x[j] = np.reshape(np.array(n_adc[jj]), -1)
                    y[j] = np.reshape(np.array(t_adc[jj]), -1)
                    tt[j] = np.reshape(np.array(rtime[jj]), -1)
         
                    x[j] = x[j][int(bw_x[j]):25000+int(bw_x[j])]
                    y[j] = y[j][int(bw_x[j]):25000+int(bw_x[j])]
                    tt[j] = tt[j][int(bw_x[j]):25000+int(bw_x[j])]

                    for ii in range(len(x[j])) :
                        ch[j-1][ii] = y[j][ii]
                        ch[j][ii] = x[j][ii]
                        t[j-1][ii] = tt[j][ii]
                        t[j][ii] = tt[j][ii]
                
                    tr.Fill()
                    
                    #initialize parameter
                    for ii in range(len(x[j])) : 
                        ch[j-1][ii] = 0
                        ch[j][ii] = 0
                        t[j-1][ii] = 0
                        t[j][ii] = 0

                    trg[j] = False
                    bw_x[j] = 0
                    trgon[j][0] = 0
                    trgon[j-1][0] = 0
                    x[j] = []
                    y[j] = []
                    tt[j] = []
                    #ch[j] = np.zeros(25000)
                    #ch[j-1] = np.zeros(25000)
    
    tr.Write()
    fout.Close()

