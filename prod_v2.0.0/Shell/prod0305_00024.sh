##!/bin/bash
    
path=/data/AMoRE/users/kimwootae/work/version_test/test_data/test_0305_00024.root
subrunNum=24
source /share/amore/anaconda3/etc/profile.d/conda.sh
source /share/amore/ROOT/6.14.04-v01.01/bin/thisroot.sh

conda activate v01
    
work_dir=/data/AMoRE/users/kimwootae/work/version_test/unpack_v2/prod_v2.0.0/
executable_name="prod_v2.py"
log_file=/data/AMoRE/users/kimwootae/work/version_test/unpack_v2/prod_v2.0.0./LOGS/test_24.log
python /data/AMoRE/users/kimwootae/work/version_test/unpack_v2/prod_v2.0.0/prod_v2.py /data/AMoRE/users/kimwootae/work/version_test/test_data/test_0305_00024.root 24

