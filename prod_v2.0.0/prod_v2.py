#!/share/amore/anaconda3/envs/v01/bin/python

import re
import time
import ROOT
import sys
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
import pandas as pd

from scipy.optimize import *
from scipy.integrate import *
from scipy.signal import *
from array import array

#user-defined function
from PRODLib import FuncLib as Wtf

start_execute = time.time()

var1 = sys.argv[1]
var2 = sys.argv[2]

path = var1
subrunNum = var2

#For butterworth bandpass filter
sampling_rate = 100e3
tbin = 1./sampling_rate
conversion_factor = 10/2**18

hlowcut = 30
hhighcut = 220
llowcut = 200
lhighcut = 500

#Value Define
nch = 16
nmodule = int(nch/2)
base_start = 7000
base_end = 8000
avg_term = 5


#myFile_a_out = ROOT.TFile("/data/AMoRE/users/kimwootae/work/test_data/light_{0}_{1}.root".format(input_file, input_number),"recreate")
runNum = '371'
myFile_a_out = ROOT.TFile("/data/AMoRE/users/kimwootae/work/version_test/test_data/PROD/PROD_heat_{0}_{1:05d}.root".format(runNum, int(subrunNum)),"recreate")
#myFile_a_out = ROOT.TFile("test.root","recreate")

tr = ROOT.TTree("tr","tr")

##EVENT ARRAY FOR BRANCH##
evt = array("i", [0])

#heat channel array define
hpeak = [[] for _ in range(nmodule)]
xhpeak = [[] for _ in range(nmodule)]
hbaseline = [[] for _ in  range(nmodule)]
hbaselineRMS = [[] for _ in range(nmodule)]
hendline = [[] for _ in range(nmodule)]
hendlineRMS = [[] for _ in range(nmodule)]
hmin = [[] for _ in range(nmodule)]

#heat filter channel array define
hfpeak = [[] for _ in range(nmodule)]
xhfpeak = [[] for _ in range(nmodule)]
hfbaseline = [[] for _ in range(nmodule)]
hfbaselineRMS = [[] for _ in range(nmodule)]
hfmin = [[] for _ in range(nmodule)]
xhfmin = [[] for _ in range(nmodule)]
hfpeak2nd = [[] for _ in range(nmodule)]
xhfpeak2nd = [[] for _ in range(nmodule)]

#light channel array define
lpeak = [[] for _ in range(nmodule)]
xlpeak = [[] for _ in range(nmodule)]
lbaseline = [[] for _ in range(nmodule)]
lbaselineRMS = [[] for _ in range(nmodule)]
lmin = [[] for _ in range(nmodule)]

#light filter channel array define
lfpeak = [[] for _ in range(nmodule)]
xlfpeak = [[] for _ in range(nmodule)]
lfbaseline = [[] for _ in range(nmodule)]
lfbaselineRMS = [[] for _ in range(nmodule)]
lendline = [[] for _ in range(nmodule)]
lendlineRMS = [[] for _ in range(nmodule)]
lfmin = [[] for _ in range(nmodule)]
xlfmin = [[] for _ in range(nmodule)]
lfpeak2nd = [[] for _ in range(nmodule)]
xlfpeak2nd = [[] for _ in range(nmodule)]

#light filter channel array define 4-order filter
lfpeak1 = [[] for _ in range(nmodule)]
xlfpeak1 = [[] for _ in range(nmodule)]
lfbaseline1 = [[] for _ in range(nmodule)]
lfbaselineRMS1 = [[] for _ in range(nmodule)]
lendline1 = [[] for _ in range(nmodule)]
lendlineRMS1 = [[] for _ in range(nmodule)]
lfmin1 = [[] for _ in range(nmodule)]
xlfmin1 = [[] for _ in range(nmodule)]
lfpeak2nd1 = [[] for _ in range(nmodule)]
xlfpeak2nd1 = [[] for _ in range(nmodule)]

#trigger time array define
trgtime = [[] for _ in range(nmodule)]

#risetime array define
h_rt90 = [[] for _ in range(nmodule)]
h_rt50 = [[] for _ in range(nmodule)]
h_rt10 = [[] for _ in range(nmodule)]

h_rtpeak90 = [[] for _ in range(nmodule)]
h_rtpeak50 = [[] for _ in range(nmodule)]
h_rtpeak10 = [[] for _ in range(nmodule)]

l_rt90 = [[] for _ in range(nmodule)]
l_rt50 = [[] for _ in range(nmodule)]
l_rt10 = [[] for _ in range(nmodule)]

l_rtpeak90 = [[] for _ in range(nmodule)]
l_rtpeak50 = [[] for _ in range(nmodule)]
l_rtpeak10 = [[] for _ in range(nmodule)]

#for amore-pilot
amore_lpeak = [[] for _ in range(nmodule)]
amore_xlpeak = [[] for _ in range(nmodule)]
diffmax = [[] for _ in range(nmodule)]
gradmax = [[] for _ in range(nmodule)]
diffmaxx = [[] for _ in range(nmodule)]
gradmaxx = [[] for _ in range(nmodule)]


#template fitting
S_a = [[] for _ in range(nmodule)]
chi_a = [[] for _ in range(nmodule)]

#trigger Or/Not
trgon = [[] for _ in range(nmodule)]

for i in range(nmodule) :
    hpeak[i] = array("f", [0])
    xhpeak[i] = array("f", [0])
    hbaseline[i] = array("f", [0])
    hbaselineRMS[i] = array("f", [0])
    hendline[i] = array("f", [0])
    hendlineRMS[i] = array("f", [0])
    hmin[i] = array("f", [0])

    #heat filter channel array define
    hfpeak[i] = array("f", [0])
    xhfpeak[i] = array("f", [0])
    hfbaseline[i] = array("f", [0])
    hfbaselineRMS[i] = array("f", [0])
    hfmin[i] = array("f", [0])
    xhfmin[i] = array("f", [0])
    hfpeak2nd[i] = array("f", [0])
    xhfpeak2nd[i] = array("f", [0])

    #light channel array define
    lpeak[i] = array("f", [0])
    xlpeak[i] = array("f", [0])
    lbaseline[i] = array("f", [0])
    lbaselineRMS[i] = array("f", [0])
    lmin[i] = array("f", [0])
    
    #light filter channel array define
    lfpeak[i] = array("f", [0])
    xlfpeak[i] = array("f", [0])
    lfbaseline[i] = array("f", [0])
    lfbaselineRMS[i] = array("f", [0])
    lendline[i] = array("f", [0])
    lendlineRMS[i] = array("f", [0])
    lfmin[i] = array("f", [0])
    xlfmin[i] = array("f", [0])
    lfpeak2nd[i] = array("f", [0])
    xlfpeak2nd[i] = array("f", [0])
    
    #light filter channel array define 4-order filter
    lfpeak1[i] = array("f", [0])
    xlfpeak1[i] = array("f", [0])
    lfbaseline1[i] = array("f", [0])
    lfbaselineRMS1[i] = array("f", [0])
    lendline1[i] = array("f", [0])
    lendlineRMS1[i] = array("f", [0])
    lfmin1[i] = array("f", [0])
    xlfmin1[i] = array("f", [0])
    lfpeak2nd1[i] = array("f", [0])
    xlfpeak2nd1[i] = array("f", [0])

    #trigger time array define
    trgtime[i] = array("f", [0])
    
    #risetime array define
    h_rt90[i] = array("f", [0])
    h_rt50[i] = array("f", [0])
    h_rt10[i] = array("f", [0])
    
    h_rtpeak90[i] = array("f", [0])
    h_rtpeak50[i] = array("f", [0])
    h_rtpeak10[i] = array("f", [0])
    
    l_rt90[i] = array("f", [0])
    l_rt50[i] = array("f", [0])
    l_rt10[i] = array("f", [0])
    
    l_rtpeak90[i] = array("f", [0])
    l_rtpeak50[i] = array("f", [0])
    l_rtpeak10[i] = array("f", [0])
    
    #for amore-pilot
    amore_lpeak[i] = array('f', [0])
    amore_xlpeak[i] = array('f', [0])
    diffmax[i] = array('f', [0])
    gradmax[i] = array('f', [0])
    diffmaxx[i] = array('f', [0])
    gradmaxx[i] = array('f', [0])

    #template fitting
    S_a[i] = array("f", [0])
    chi_a[i] = array("f", [0])
    
    ##DEFINITION TBRANCH##

    tr.Branch("evt", evt, "evt/I")

    #heat channel TBranch
    tr.Branch("hpeak_{0}".format(i), hpeak[i], "hpeak_{0}/F".format(i))
    tr.Branch("xhpeak_{0}".format(i), xhpeak[i], "xhpeak_{0}/F".format(i))
    tr.Branch("hbaseline_{0}".format(i), hbaseline[i], "hbaseline_{0}/F".format(i))
    tr.Branch("hbaselineRMS_{0}".format(i), hbaselineRMS[i], "hbaselineRMS_{0}/F".format(i))
    tr.Branch("hendline_{0}".format(i), hendline[i], "hendline_{0}/F".format(i))
    tr.Branch("hendlineRMS_{0}".format(i), hendlineRMS[i], "hendlineRMS_{0}/F".format(i))
    tr.Branch("hmin_{0}".format(i), hmin[i], "hmin_{0}/F".format(i))
    
    #heat filter channel TBranch
    tr.Branch("hfpeak_{0}".format(i), hfpeak[i], "hfpeak_{0}/F".format(i))
    tr.Branch("xhfpeak_{0}".format(i), xhfpeak[i], "xhfpeak_{0}/F".format(i))
    tr.Branch("hfbaseline_{0}".format(i), hfbaseline[i], "hfbaseline_{0}/F".format(i))
    tr.Branch("hfbaselineRMS_{0}".format(i), hfbaselineRMS[i], "hfbaselineRMS_{0}/F".format(i))
    tr.Branch("hfmin_{0}".format(i), hfmin[i], "hfmin_{0}/F".format(i))
    tr.Branch("xhfmin_{0}".format(i), xhfmin[i], "xhfmin_{0}/F".format(i))
    tr.Branch("hfpeak2nd_{0}".format(i), hfpeak2nd[i], "hfpeak2nd_{0}/F".format(i))
    tr.Branch("xhfpeak2nd_{0}".format(i), xhfpeak2nd[i], "xhfpeak2nd_{0}/F".format(i))
    
    #light channel TBranch
    tr.Branch("lpeak_{0}".format(i), lpeak[i], "lpeak_{0}/F".format(i))
    tr.Branch("xlpeak_{0}".format(i), xlpeak[i], "xlpeak_{0}/F".format(i))
    tr.Branch("lbaseline_{0}".format(i), lbaseline[i], "lbaseline_{0}/F".format(i))
    tr.Branch("lbaselineRMS_{0}".format(i), lbaselineRMS[i], "lbaselineRMS_{0}/F".format(i))
    tr.Branch("lmin_{0}".format(i), lmin[i], "lmin_{0}/F".format(i))
    
    #light filter channel TBranch
    tr.Branch("lfpeak_{0}".format(i), lfpeak[i], "lfpeak_{0}/F".format(i))
    tr.Branch("xlfpeak_{0}".format(i), xlfpeak[i], "xlfpeak_{0}/F".format(i))
    tr.Branch("lfbaseline_{0}".format(i), lfbaseline[i], "lfbaseline_{0}/F".format(i))
    tr.Branch("lfbaselineRMS_{0}".format(i), lfbaselineRMS[i], "lfbaselineRMS_{0}/F".format(i))
    tr.Branch("lendline_{0}".format(i), lendline[i], "lendline_{0}/F".format(i))
    tr.Branch("lendlineRMS_{0}".format(i), lendlineRMS[i], "lendlineRMS_{0}/F".format(i))
    tr.Branch("lfmin_{0}".format(i), lfmin[i], "lfmin_{0}/F".format(i))
    tr.Branch("xlfmin_{0}".format(i), xlfmin[i], "xlfmin_{0}/F".format(i))
    tr.Branch("lfpeak2nd_{0}".format(i), lfpeak2nd[i], "lfpeak2nd_{0}/F".format(i))
    tr.Branch("xlfpeak2nd_{0}".format(i), xlfpeak2nd[i], "xlfpeak2nd_{0}/F".format(i))
    
    #light filter channel TBranch 4-oreder filter
    tr.Branch("lfpeak1_{0}".format(i), lfpeak1[i], "lfpeak1_{0}/F".format(i))
    tr.Branch("xlfpeak1_{0}".format(i), xlfpeak1[i], "xlfpeak1_{0}/F".format(i))
    tr.Branch("lfbaseline1_{0}".format(i), lfbaseline1[i], "lfbaseline1_{0}/F".format(i))
    tr.Branch("lfbaselineRMS1_{0}".format(i), lfbaselineRMS1[i], "lfbaselineRMS1_{0}/F".format(i))
    tr.Branch("lendline1_{0}".format(i), lendline1[i], "lendline1_{0}/F".format(i))
    tr.Branch("lendlineRMS1_{0}".format(i), lendlineRMS1[i], "lendlineRMS1_{0}/F".format(i))
    tr.Branch("lfmin1_{0}".format(i), lfmin1[i], "lfmin1_{0}/F".format(i))
    tr.Branch("xlfmin1_{0}".format(i), xlfmin1[i], "xlfmin1_{0}/F".format(i))
    tr.Branch("lfpeak2nd1_{0}".format(i), lfpeak2nd1[i], "lfpeak2nd1_{0}/F".format(i))
    tr.Branch("xlfpeak2nd1_{0}".format(i), xlfpeak2nd1[i], "xlfpeak2nd1_{0}/F".format(i))
     
    #trigger time TBranch
    tr.Branch("trgtime_{0}".format(i), trgtime[i], "trgtime_{0}/F".format(i))
    
    #risetime TBranch
    tr.Branch("h_rt90_{0}".format(i), h_rt90[i], "h_rt90_{0}/F".format(i))
    tr.Branch("h_rt50_{0}".format(i), h_rt50[i], "h_rt50_{0}/F".format(i))
    tr.Branch("h_rt10_{0}".format(i), h_rt10[i], "h_rt10_{0}/F".format(i))
    
    tr.Branch("h_rtpeak90_{0}".format(i), h_rtpeak90[i], "h_rtpeak90_{0}/F".format(i))
    tr.Branch("h_rtpeak50_{0}".format(i), h_rtpeak50[i], "h_rtpeak50_{0}/F".format(i))
    tr.Branch("h_rtpeak10_{0}".format(i), h_rtpeak10[i], "h_rtpeak10_{0}/F".format(i))
    
    tr.Branch("l_rt90_{0}".format(i), l_rt90[i], "l_rt90_{0}/F".format(i))
    tr.Branch("l_rt50_{0}".format(i), l_rt50[i], "l_rt50_{0}/F".format(i))
    tr.Branch("l_rt10_{0}".format(i), l_rt10[i], "l_rt10_{0}/F".format(i))
    
    tr.Branch("l_rtpeak90_{0}".format(i), l_rtpeak90[i], "l_rtpeak90_{0}/F".format(i))
    tr.Branch("l_rtpeak50_{0}".format(i), l_rtpeak50[i], "l_rtpeak50_{0}/F".format(i))
    tr.Branch("l_rtpeak10_{0}".format(i), l_rtpeak10[i], "l_rtpeak10_{0}/F".format(i))
    
    #for amore-pilot
    tr.Branch("amore_lpeak_{0}".format(i), amore_lpeak[i], "amore_lpeak_{0}/F".format(i))
    tr.Branch("amore_xlpeak_{0}".format(i), amore_xlpeak[i], "amore_xlpeak_{0}/F".format(i))
    tr.Branch("diffmax_{0}".format(i), diffmax[i], "diffmax_{0}/F".format(i))
    tr.Branch("diffmaxx_{0}".format(i), diffmaxx[i], "diffmaxx_{0}/F".format(i))
    tr.Branch("gradmax_{0}".format(i), gradmax[i], "gradmax_{0}/F".format(i))
    tr.Branch("gradmaxx_{0}".format(i), gradmaxx[i], "gradmaxx_{0}/F".format(i))
    
    #template fitting
    tr.Branch("S_a_{0}".format(i), S_a[i], "S_a_{0}/F".format(i))
    tr.Branch("chi_a_{0}".format(i), chi_a[i], "chi_a_{0}/F".format(i))

    trgon[i] = array("i", [0])
    tr.Branch("trgon_{0}".format(i), trgon[i], "trgon_{0}/I".format(i))

###TEMPATE FIITNG###
#fevt_a =pd.read_csv("/data/AMoRE/users/kimwootae/work/amoretest/read_raw_and_pyroot/pyroot/2.6MeV_gamma_evt_template_30mK_temporary_2e+4.txt")
#tmp_axx = fevt_a.x.values
#tmp_ayy = fevt_a.y.values
#tmp_axx = np.arange(len(tmp_axx))
#tmp_base = np.mean(tmp_ayy[base_start:base_end])
#tmp_baseRMS = np.mean(tmp_ayy[base_start:base_end])
#a = Wtf.heat_ana_temp(tmp_ayy, conversion_factor, sampling_rate, tmp_base, tmp_baseRMS, base_end)
#xpeak10_b = int(a[-1])

###PRODUCTION###
tr_input = Wtf.pyroot_file_read(path)
print(path)
nentries = tr_input.GetEntries()
print (nentries)

for i in range(nentries) :
    
    if (i%100==0 and i>0) :
        inter_execute = time.time()
        inter_run_time = inter_execute - start_execute
        print ("{0}, run time:{1}".format(i, inter_run_time))
    
    evtnumber = i
    evt[0] = evtnumber
    ch_light, ch_heat, time_light, time_heat, base_light, base_heat, baseRMS_light, baseRMS_heat, trgON = Wtf.pyroot_read(tr_input, evtnumber, base_start, base_end, sampling_rate)
    for ii in range(nmodule) :
    #for ii in range() :
        trgon[ii][0] = trgON[ii]
        temp_trgon = np.mean(ch_heat[ii]) #temporary trgon Or/Not 
        if(temp_trgon == 0) : continue
        trgtime[ii][0] = time_heat[ii][0]
    
        avg_heat = Wtf.movingaverage(ch_heat[ii], avg_term)
        avg_heat = np.append([ch_heat[ii][0]]*avg_term, avg_heat[avg_term:])
        avg_heat = np.append(avg_heat[:-avg_term], [ch_heat[ii][-avg_term]]*avg_term)
    
        ##heat channel fill
        hout = Wtf.heat_ana(avg_heat, conversion_factor, sampling_rate, base_heat[ii], baseRMS_heat[ii], base_end)
        xhpeak[ii][0] = hout[0]
        hpeak[ii][0] = hout[1] 
        h_rtpeak90[ii][0] = hout[2] 
        h_rtpeak50[ii][0] = hout[3]
        h_rtpeak10[ii][0] = hout[4]
        h_rt90[ii][0] = hout[5]
        h_rt50[ii][0] = hout[6]
        h_rt10[ii][0] = hout[7]
        hbaseline[ii][0] = base_heat[ii]
        hbaselineRMS[ii][0] = baseRMS_heat[ii]
        #val1[0] = hout[8]
        #val2[0] = hout[9]
        #val3[0] = hout[10]
        hendline[ii][0] = hout[11]
        hendlineRMS[ii][0] = hout[12]
        hmin[ii][0] = hout[13]
    
        ##light channel fill
        lout = Wtf.light_ana(ch_light[ii], conversion_factor, sampling_rate, base_light[ii], baseRMS_light[ii], base_end)
        xlpeak[ii][0] = lout[0]
        lpeak[ii][0] = lout[1]
        l_rtpeak90[ii][0] = lout[2] 
        l_rtpeak50[ii][0] = lout[3]
        l_rtpeak10[ii][0] = lout[4]
        l_rt90[ii][0] = lout[5]
        l_rt50[ii][0] = lout[6]
        l_rt10[ii][0] = lout[7]
        lbaseline[ii][0] = base_light[ii]
        lbaselineRMS[ii][0] = baseRMS_light[ii]
        lendline[ii][0] = lout[11]
        lendlineRMS[ii][0] = lout[12]
        lmin[ii][0] = lout[13]
        
        ##light channel fill for amore
        lout1 = Wtf.light_ana_amore(ch_light[ii], conversion_factor, sampling_rate, 7500, 10000)
        print (lout1)
        amore_xlpeak[ii][0] = lout1[0]
        amore_lpeak[ii][0] = lout1[1]
        diffmax[ii][0] = lout1[2]
        gradmax[ii][0] = lout1[3]
        diffmaxx[ii][0] = lout1[4]
        gradmaxx[ii][0] = lout1[5]

        ##heat channel filter fill
        hfout = Wtf.heat_filter_ana(ch_heat[ii]-ch_heat[ii][0], conversion_factor, sampling_rate, base_end)
        xhfpeak[ii][0] = hfout[0]
        hfpeak[ii][0] = hfout[1]
        hfbaseline[ii][0] = hfout[2]
        hfbaselineRMS[ii][0] = hfout[3]
        xhfmin[ii][0] = hfout[4]
        hfmin[ii][0] = hfout[5]
        xhfpeak2nd[ii][0] = hfout[6]
        hfpeak2nd[ii][0] = hfout[7]

        ##light channel filter fill
        lfout = Wtf.light_filter_ana(ch_light[ii]-ch_light[ii][0], conversion_factor, sampling_rate, base_end)
        xlfpeak[ii][0] = lfout[0]
        lfpeak[ii][0] = lfout[1]
        lfbaseline[ii][0] = lfout[2]
        lfbaselineRMS[ii][0] = lfout[3]
        xlfmin[ii][0] = lfout[4]
        lfmin[ii][0] = lfout[5]
        xlfpeak2nd[ii][0] = lfout[6]
        lfpeak2nd[ii][0] = lfout[7]
        
        ##light channel filter fill 4-order filter
        lfout1 = Wtf.light_filter_ana1(ch_light[ii]-ch_light[ii][0], conversion_factor, sampling_rate, base_end)
        xlfpeak1[ii][0] = lfout1[0]
        lfpeak1[ii][0] = lfout1[1]
        lfbaseline1[ii][0] = lfout1[2]
        lfbaselineRMS1[ii][0] = lfout1[3]
        xlfmin1[ii][0] = lfout1[4]
        lfmin1[ii][0] = lfout1[5]
        xlfpeak2nd1[ii][0] = lfout1[6]
        lfpeak2nd1[ii][0] = lfout1[7]

        ##Template fitting fill

        ##Template fitting fill
        #tempout = Wtf.template_fitting(np.arange(len(time_heat[ii])), (ch_heat[ii]-ch_heat[ii][0])*conversion_factor, tmp_axx, tmp_ayy, 0, 8000, xpeak10_b, hout[4])
        #S_a[ii][0] = tempout[0]
        #chi_a[ii][0] = tempout[1]
    
    tr.Fill()
    
myFile_a_out.Write()
myFile_a_out.Close()
    
end_execute =time.time()
run_time = end_execute - start_execute

print ("{0}".format(run_time))
